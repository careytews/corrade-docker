#    Copyright 2018, Carissima Resident @ Second Life

#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at

#        http://www.apache.org/licenses/LICENSE-2.0

#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

VERSION=10.2.7048.41804
SHORT_VERSION=10.2

all: build

build:
	# Build the base image without the configuration
	docker build --build-arg CORRADE_VERSION=${VERSION} \
		-f Dockerfile.continuous \
		-t carissima/corrade-continuous\:latest \
		-t carissima/corrade-continuous\:${SHORT_VERSION} \
		-t carissima/corrade-continuous\:${VERSION} .

push:
	# Push to the public repo
	docker push carissima/corrade-continuous\:${VERSION}
	docker push carissima/corrade-continuous\:${SHORT_VERSION}
	docker push carissima/corrade-continuous\:latest

notice:
	echo "@notice ([Corrade 10 Continuous Docker Container]) (The Corrade 10 [Continuous Branch] ${VERSION} Docker container is now available! Get 'er while she's hot! https://hub.docker.com/r/carissima/corrade-continuous)" > notice.continuous.${VERSION}.txt
