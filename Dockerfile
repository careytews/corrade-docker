#    Copyright 2018, Carissima Resident @ Second Life

#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at

#        http://www.apache.org/licenses/LICENSE-2.0

#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

FROM carissima/mono-complete:latest

ARG CORRADE_VERSION

LABEL maintainer writer.cody@gmail.com

RUN apt-get update && apt-get install -y wget unzip procps nano

RUN mkdir /corrade

WORKDIR /corrade

RUN wget http://corrade.grimore.org/download/continuous/corrade/Corrade-${CORRADE_VERSION}.zip && unzip *.zip

RUN rm -rf Cache && rm -rf State && rm -rf Logs && rm -rf Databases

ADD *.sh /corrade/
